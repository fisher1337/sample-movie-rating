# Sample Movie Rating service

Movie rating service. Backend written in Java (Spring), frontend in React.js. Backed by Mongo.DB.

## Requirements

-   make
-   docker 18.09
-   docker-compose 1.23.1
-   JDK 8
-   NPM 6.4.1
-   Node.js 10.15.2

## Run in development

To run development dependencies (Mongo.DB, mongo-express) use `make dev-env`.

Port mappings:

-   Mongo.DB: `27017`
-   mongo-express: `8081`

## Tests

-   All tests: `make test`
-   Only backend tests: `make test-be`
-   Only frontend tests: `make test-fe`

## Build

-   Build docker containers: `make build-docker`

## Run

-   Run all: `make run`
-   Run only backend: `make run-be`
-   Run only frontend: `make run-fe`

The application will be running on `http://localhost:8000`.
