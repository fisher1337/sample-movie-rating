COMPOSE := $(shell command -v docker-compose 2> /dev/null)
NPM := $(shell command -v npm 2> /dev/null)

all:
ifndef COMPOSE
    $(error "docker-compose is required")
endif
ifndef NPM
    $(error "npm is required")
endif

dev-env:
	docker-compose -f ./deployments/compose-dev.yml -p movie-rating up -d

test-be:
	cd backend && ./gradlew test

test-fe:
	cd frontend && CI=true npm test

test: test-be test-fe

build:
	cd backend && ./gradlew build
	cd frontend && npm install && npm run build

build-docker: build
	docker-compose -f ./deployments/compose-be.yml -p movie-rating build
	docker-compose -f ./deployments/compose-fe.yml -p movie-rating build

run-be: dev-env build
	docker-compose -f ./deployments/compose-be.yml -p movie-rating up -d

run-fe: dev-env build
	docker-compose -f ./deployments/compose-fe.yml -p movie-rating up -d

run: run-be run-fe
