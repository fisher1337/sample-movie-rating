import { LOAD_MOVIES_REQUEST, RATE_MOVIE_REQUEST } from "./types";

export const loadMovies = () => ({
  type: LOAD_MOVIES_REQUEST
});

export const rateMovie = (movieId, rating) => ({
  type: RATE_MOVIE_REQUEST,
  movieId: movieId,
  rating: rating
});
