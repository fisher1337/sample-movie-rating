import {
  RATE_MOVIE_REQUEST,
  RATE_MOVIE_SUCCESS,
  RATE_MOVIE_FAILURE
} from "../actions/types";
import { takeLatest, call, put } from "redux-saga/effects";
import axios from "axios";

const rateMovieSaga = [takeLatest(RATE_MOVIE_REQUEST, workerSaga)];

function postRating(movieId, rating) {
  return axios({
    method: "post",
    url: `/api/v1/movie/${movieId}/rating`,
    data: {
      rating: rating
    }
  });
}

function* workerSaga(action) {
  try {
    const response = yield call(postRating, action.movieId, action.rating);
    const movie = response.data;

    yield put({ type: RATE_MOVIE_SUCCESS, movie });
  } catch (error) {
    yield put({ type: RATE_MOVIE_FAILURE, error });
  }
}

export default rateMovieSaga;
