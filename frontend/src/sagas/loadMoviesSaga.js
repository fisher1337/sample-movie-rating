import {
  LOAD_MOVIES_REQUEST,
  LOAD_MOVIES_SUCCESS,
  LOAD_MOVIES_FAILURE
} from "../actions/types";
import { takeLatest, call, put, delay } from "redux-saga/effects";
import axios from "axios";

const loadMoviesSaga = [takeLatest(LOAD_MOVIES_REQUEST, workerSaga)];

function fetchMovies() {
  return axios({
    method: "get",
    url: "/api/v1/movie"
  });
}

function* workerSaga() {
  try {
    const response = yield call(fetchMovies);
    const movies = response.data;

    yield delay(1000);
    yield put({ type: LOAD_MOVIES_SUCCESS, movies });
  } catch (error) {
    yield put({ type: LOAD_MOVIES_FAILURE, error });
  }
}

export default loadMoviesSaga;
