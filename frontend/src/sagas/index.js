import { all } from "redux-saga/effects";
import loadMoviesSaga from "./loadMoviesSaga";
import rateMovieSaga from "./rateMovieSaga";

export default function* watcherSaga() {
  yield all([...loadMoviesSaga, ...rateMovieSaga]);
}
