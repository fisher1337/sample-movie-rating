import React from "react";

const Error = () => {
  return <h3>Error while fetching data</h3>;
};

export default Error;
