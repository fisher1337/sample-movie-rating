import "./item.css";
import React from "react";

import { Button, Header, Icon, Rating, Modal } from "semantic-ui-react";

import dateformat from "dateformat";

const icons = {
  "sci-fi": "flask",
  action: "motorcycle",
  drama: "bug"
};

function parseDate(date) {
  return dateformat(new Date(date), "dd-mm-yyyy");
}

function parseScore(score) {
  if (score) {
    return parseFloat(score).toFixed(2);
  } else {
    return "No rating";
  }
}

function parseGenre(genre) {
  let icon = icons[genre.toLowerCase()];

  return <i className={`${icon} icon`} />;
}

class MovieItem extends React.Component {
  state = { open: false, rating: 5 };
  show = dimmer => () => this.setState({ dimmer, open: true });
  close = () => this.setState({ open: false });
  sendRating = () => {
    this.props.rateMovie(this.props.movie.id, this.state.rating);
    this.setState({ open: false });
  };
  onRate = (e, data) => {
    this.setState({ rating: data.rating });
  };

  renderModal() {
    const { open, dimmer } = this.state;

    return (
      <Modal
        className="rate-modal"
        dimmer={dimmer}
        open={open}
        onClose={this.close}
      >
        <Header content={"Rate " + this.props.movie.title} />
        <Modal.Content>
          <Rating
            maxRating={10}
            size="huge"
            defaultRating={this.state.rating}
            icon="star"
            onRate={this.onRate}
          />
        </Modal.Content>
        <Modal.Actions>
          <Button color="green" onClick={this.sendRating}>
            <Icon name="checkmark" /> Save
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }

  render() {
    return (
      <div className="item movie-item">
        {this.renderModal()}
        <div className="content">
          <h2>{this.props.movie.title}</h2>
          <p>
            Genre: {parseGenre(this.props.movie.genre)} | Release date:{" "}
            {parseDate(this.props.movie.releaseDate)} | Rating:{" "}
            <b>{parseScore(this.props.movie.score)}</b>
          </p>
          <Button primary floated="right" onClick={this.show(true)}>
            Rate movie
          </Button>
        </div>
      </div>
    );
  }
}

export default MovieItem;
