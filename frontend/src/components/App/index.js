import React, { Component } from "react";

import Spinner from "../Spinner";
import Error from "../Error";
import MovieList from "../../containers/MovieListContainer";

class App extends Component {
  componentDidMount() {
    this.props.loadMovies();
  }
  renderContent() {
    if (this.props.loading) {
      return <Spinner />;
    } else if (this.props.error) {
      return <Error />;
    } else {
      return <MovieList />;
    }
  }
  render() {
    return <div className="ui container">{this.renderContent()}</div>;
  }
}

export default App;
