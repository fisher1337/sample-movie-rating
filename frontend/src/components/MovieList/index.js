import React from "react";
import MovieItem from "../../containers/MovieItemContainer";

class MovieList extends React.Component {
  renderItems = movies =>
    movies.map(movie => <MovieItem movie={movie} key={movie.id} />);

  render() {
    return (
      <div className="ui relaxed divided items">
        {this.renderItems(this.props.movies)}
      </div>
    );
  }
}

export default MovieList;
