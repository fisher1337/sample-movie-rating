import {
  LOAD_MOVIES_REQUEST,
  LOAD_MOVIES_SUCCESS,
  LOAD_MOVIES_FAILURE,
  RATE_MOVIE_SUCCESS
} from "../actions/types";

const initialState = {
  loading: false,
  movies: [],
  error: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOAD_MOVIES_REQUEST:
      return { ...state, loading: true, error: null };
    case LOAD_MOVIES_SUCCESS:
      return { ...state, loading: false, movies: action.movies };
    case LOAD_MOVIES_FAILURE:
      return { ...state, loading: false, error: action.error };
    case RATE_MOVIE_SUCCESS:
      return {
        ...state,
        movies: state.movies.map(item =>
          item.id === action.movie.id
            ? { ...item, score: action.movie.score }
            : item
        )
      };
    default:
      return state;
  }
}
