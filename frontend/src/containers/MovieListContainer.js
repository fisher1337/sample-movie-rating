import { connect } from "react-redux";
import MovieList from "../components/MovieList";

function mapStateToProps({ movie }) {
  return { movies: movie.movies };
}

export default connect(mapStateToProps)(MovieList);
