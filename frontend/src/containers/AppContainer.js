import { connect } from "react-redux";
import * as actions from "../actions";

import App from "../components/App";

function mapStateToProps({ movie }) {
  return movie;
}

export default connect(
  mapStateToProps,
  actions
)(App);
