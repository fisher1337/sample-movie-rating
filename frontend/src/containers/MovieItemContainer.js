import MovieItem from "../components/MovieItem";
import { connect } from "react-redux";
import * as actions from "../actions";

export default connect(
  null,
  actions
)(MovieItem);
