db = connect("localhost:27017/movie-rating");

let djangoId = ObjectId();
let djangoRate1Id = ObjectId();
let djangoRate2Id = ObjectId();

db.movie.insert([
  {
    title: "Back to the Future",
    genre: "Sci-Fi",
    releaseDate: new Date("1985-12-04"),
    score: null,
    numberOfRatings: 0,
    ratings: []
  },
  {
    title: "The Matrix: Reloaded",
    genre: "Sci-Fi",
    releaseDate: new Date("2013-05-07"),
    score: null,
    numberOfRatings: 0,
    ratings: []
  },
  {
    title: "Casino Royale",
    genre: "Action",
    releaseDate: new Date("2006-11-14"),
    score: null,
    numberOfRatings: 0,
    ratings: []
  },
  {
    title: "Star Wars: Episode IV - A New Hope",
    genre: "Action",
    releaseDate: new Date("1978-01-29"),
    score: null,
    numberOfRatings: 0,
    ratings: []
  },
  {
    title: "American Sniper",
    genre: "Drama",
    releaseDate: new Date("2014-11-11"),
    score: null,
    numberOfRatings: 0,
    ratings: []
  },
  {
    _id: djangoId,
    title: "Django Unchained",
    genre: "Drama",
    releaseDate: new Date("2013-01-18"),
    score: 5.0,
    numberOfRatings: 2,
    ratings: [
      {
        $id: djangoRate1Id,
        $ref: "rating"
      },
      {
        $id: djangoRate2Id,
        $ref: "rating"
      }
    ]
  }
]);

db.rating.insert([
  {
    _id: djangoRate1Id,
    movieId: djangoId,
    rating: 10
  },
  {
    _id: djangoRate2Id,
    movieId: djangoId,
    rating: 0
  }
]);
