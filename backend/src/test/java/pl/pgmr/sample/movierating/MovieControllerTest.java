package pl.pgmr.sample.movierating;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.pgmr.sample.movierating.controller.MovieController;
import pl.pgmr.sample.movierating.dto.MovieDto;
import pl.pgmr.sample.movierating.dto.RatingDto;
import pl.pgmr.sample.movierating.model.Movie;
import pl.pgmr.sample.movierating.model.Rating;
import pl.pgmr.sample.movierating.repository.MovieRepository;
import pl.pgmr.sample.movierating.repository.RatingRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MovieRatingApplication.class)
@WebAppConfiguration
public class MovieControllerTest {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private RatingRepository ratingRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    private final ObjectId testMovie1Id = new ObjectId();
    private final ObjectId rating1Id = new ObjectId();
    private final ObjectId rating2Id = new ObjectId();
    private List<Rating> testRatings = Arrays.asList(
            new Rating(rating1Id, testMovie1Id, 10),
            new Rating(rating2Id, testMovie1Id, 0)
    );
    private List<RatingDto> testRatingDtos = Arrays.asList(
            new RatingDto(rating1Id.toHexString(), testMovie1Id.toHexString(), 10),
            new RatingDto(rating2Id.toHexString(), testMovie1Id.toHexString(), 0)
    );
    private List<Movie> testMovies = Arrays.asList(
            new Movie(testMovie1Id, "test1", "test", new Date(), 5.0, testRatings.size(), testRatings),
            new Movie(null, "test2", "test", new Date(), null, 0, new ArrayList<>())
    );

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        ratingRepository.saveAll(testRatings);
        movieRepository.saveAll(testMovies);
    }

    @After
    public void tearDown() throws Exception {
        ratingRepository.deleteAll(testRatings);
        movieRepository.deleteAll(testMovies);
    }

    @Test
    public void getAllMovies() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/v1/movie")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        List<MovieDto> results = objectMapper.readValue(result.getResponse().getContentAsString(),
                new TypeReference<List<MovieDto>>(){});

        assertThat(results.size()).isGreaterThan(testMovies.size());
    }

    @Test
    public void getMovieRating() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/v1/movie/{id}/rating", testMovie1Id.toHexString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        List<RatingDto> results = objectMapper.readValue(result.getResponse().getContentAsString(),
                new TypeReference<List<RatingDto>>(){});

        assertThat(results.size()).isEqualTo(testRatings.size());
        assertThat(results).containsAll(testRatingDtos);
    }

    @Test
    public void getMovieRating_NoSuchMovieReturns404() throws Exception {
        mockMvc.perform(get("/api/v1/movie/123/rating")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void postNewRating() throws Exception {
        RatingDto newRating = new RatingDto(null, null, 2);
        MvcResult result = mockMvc.perform(post("/api/v1/movie/{id}/rating", testMovie1Id.toHexString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newRating)))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        MovieDto movie = objectMapper.readValue(result.getResponse().getContentAsString(), MovieDto.class);
        assertThat(movie).isNotNull();
        assertThat(movie.getScore()).isEqualTo(4.0);
    }

    @Test
    public void postNewRating_TooLow() throws Exception {
        RatingDto newRating = new RatingDto(null, null, -1);
        mockMvc.perform(post("/api/v1/movie/{id}/rating", testMovie1Id.toHexString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newRating)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postNewRating_TooHigh() throws Exception {
        RatingDto newRating = new RatingDto(null, null, 11);
        mockMvc.perform(post("/api/v1/movie/{id}/rating", testMovie1Id.toHexString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newRating)))
                .andExpect(status().isBadRequest());
    }
}
