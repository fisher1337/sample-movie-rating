package pl.pgmr.sample.movierating.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RatingDto {
    private String id;
    private String movieId;
    @Min(0)
    @Max(10)
    private int rating;
}
