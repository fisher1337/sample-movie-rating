package pl.pgmr.sample.movierating.service.impl;


import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pgmr.sample.movierating.model.Movie;
import pl.pgmr.sample.movierating.repository.MovieRepository;
import pl.pgmr.sample.movierating.service.MovieService;

import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {
    private final MovieRepository movieRepository;

    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public List<Movie> getMovies() {
        return movieRepository.findAll();
    }

    @Override
    public Optional<Movie> getMovieById(String movieId) {
        try {
            ObjectId id = new ObjectId(movieId);
            return movieRepository.findById(id);
        } catch (IllegalArgumentException ex) {
            return Optional.empty();
        }
    }

    @Override
    public Movie saveMovie(Movie movie) {
        return movieRepository.save(movie);
    }
}
