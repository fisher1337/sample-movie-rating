package pl.pgmr.sample.movierating.service.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pgmr.sample.movierating.model.Rating;
import pl.pgmr.sample.movierating.repository.RatingRepository;
import pl.pgmr.sample.movierating.service.RatingService;

@Service
public class RatingServiceImpl implements RatingService {
    private final RatingRepository ratingRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Override
    public Rating addRating(ObjectId movieId, int rating) {
        return ratingRepository.save(new Rating(null, movieId, rating));
    }
}
