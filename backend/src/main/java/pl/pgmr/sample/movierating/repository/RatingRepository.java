package pl.pgmr.sample.movierating.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import pl.pgmr.sample.movierating.model.Rating;

public interface RatingRepository extends MongoRepository<Rating, ObjectId> {
}
