package pl.pgmr.sample.movierating.service;

import org.bson.types.ObjectId;
import pl.pgmr.sample.movierating.model.Rating;

public interface RatingService {
    Rating addRating(ObjectId movieId, int rating);
}
