package pl.pgmr.sample.movierating.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import pl.pgmr.sample.movierating.model.Movie;


public interface MovieRepository extends MongoRepository<Movie, ObjectId> {
}
