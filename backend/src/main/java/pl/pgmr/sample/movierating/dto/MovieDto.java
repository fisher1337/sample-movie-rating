package pl.pgmr.sample.movierating.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
public class MovieDto {
    private String id;
    @NotBlank
    private String title;
    @NotBlank
    private String genre;
    @NotNull
    private Date releaseDate;
    private Double score = null;
}
