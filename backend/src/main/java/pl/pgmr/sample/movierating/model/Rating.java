package pl.pgmr.sample.movierating.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
public class Rating {
    @Id
    private ObjectId id;
    @Indexed
    private ObjectId movieId;
    @Min(0)
    @Max(10)
    private int rating;
}
