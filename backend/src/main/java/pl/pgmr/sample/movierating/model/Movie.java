package pl.pgmr.sample.movierating.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class Movie {
    @Id
    private ObjectId id;
    @NotBlank
    private String title;
    @NotBlank
    private String genre;
    @NotNull
    private Date releaseDate;
    private Double score = null;
    private int numberOfRatings = 0;
    @DBRef(lazy = true)
    private List<Rating> ratings = new ArrayList<>();

    public void updateScore(Rating rating) {
        numberOfRatings++;

        if (score == null) {
            score = (double) rating.getRating();
        } else {
            score = score + (rating.getRating() - score) / numberOfRatings;
        }

        ratings.add(rating);
    }
}
