package pl.pgmr.sample.movierating.service;

import pl.pgmr.sample.movierating.model.Movie;

import java.util.List;
import java.util.Optional;

public interface MovieService {
    List<Movie> getMovies();
    Optional<Movie> getMovieById(String movieId);
    Movie saveMovie(Movie movie);
}
