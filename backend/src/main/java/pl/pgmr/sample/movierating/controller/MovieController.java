package pl.pgmr.sample.movierating.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.pgmr.sample.movierating.controller.exception.ResourceNotFoundException;
import pl.pgmr.sample.movierating.dto.MovieDto;
import pl.pgmr.sample.movierating.dto.RatingDto;
import pl.pgmr.sample.movierating.model.Movie;
import pl.pgmr.sample.movierating.model.Rating;
import pl.pgmr.sample.movierating.service.MovieService;
import pl.pgmr.sample.movierating.service.RatingService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1")
public class MovieController {
    private final MovieService movieService;
    private final RatingService ratingService;
    private final ModelMapper modelMapper;


    @Autowired
    public MovieController(MovieService movieService, RatingService ratingService, ModelMapper modelMapper) {
        this.movieService = movieService;
        this.ratingService = ratingService;
        this.modelMapper = modelMapper;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/movie")
    public List<MovieDto> getMovies() {
        return movieService.getMovies().stream()
                .map(movie -> modelMapper.map(movie, MovieDto.class))
                .collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.GET, path = "/movie/{id}/rating")
    public List<RatingDto> getRatingsForMovie(@PathVariable("id") String movieId) {
        Movie movie = movieService.getMovieById(movieId)
                .orElseThrow(ResourceNotFoundException::new);

        return movie.getRatings().stream()
                .map(rating -> modelMapper.map(rating, RatingDto.class))
                .collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST, path = "/movie/{id}/rating")
    public MovieDto postNewRating(@PathVariable("id") String movieId, @Valid @RequestBody RatingDto ratingDto) {
        Movie movie = movieService.getMovieById(movieId)
                .orElseThrow(ResourceNotFoundException::new);

        Rating rating = ratingService.addRating(movie.getId(), ratingDto.getRating());
        movie.updateScore(rating);
        movie = movieService.saveMovie(movie);

        return modelMapper.map(movie, MovieDto.class);
    }
}
